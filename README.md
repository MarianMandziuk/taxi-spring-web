## Web MVC homework
- Spring_4 Web MVC Homework
- Spring_5 Web MVC Homework
- Spring_6 Data Homework
- Spring_7 Test Homework

### Homework structure:
Branches correspond to certain homework
- **master** contains all homeworks
- **spring_4** correspond to Spring_4 Web MVC Homework
- **spring_5** correspond to Spring_5 Web MVC Homework
  - subtask with **rest-template** located at [https://gitlab.com/MarianMandziuk/spring-rest-template](https://gitlab.com/MarianMandziuk/spring-rest-template)
- **spring_6** correspond to Spring_6 Data Homework
- **spring_7** correspond to Spring_7 Test Homework
