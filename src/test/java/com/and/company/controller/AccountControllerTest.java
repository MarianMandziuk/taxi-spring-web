package com.and.company.controller;

import com.and.company.controller.assembler.AccountAssembler;
import com.and.company.controller.model.AccountModel;
import com.and.company.dto.AccountDto;
import com.and.company.helper.config.TestConfig;
import com.and.company.model.enums.ErrorType;
import com.and.company.service.AccountService;
import com.and.company.service.LoyaltyService;
import com.and.company.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static com.and.company.helper.util.TestDataUtil.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AccountController.class)
@Import(TestConfig.class)
class AccountControllerTest {
    private static final String EMAIL_VALIDATION_MESSAGE = "must be a well-formed email address";

    private static final String EMAIL_JSON_PATH = "$.email";
    private static final String NAME_JSON_PATH = "$.name";

    private static final String MESSAGE_JSON_PATH = "$[0].message";
    private static final String ERROR_TYPE_JSON_PATH = "$[0].errorType";

    @MockBean
    private AccountService accountService;

    @MockBean
    private LoyaltyService loyaltyService;

    @MockBean
    private OrderService orderService;

    @MockBean
    private AccountAssembler accountAssembler;

    @Autowired
    private MockMvc mockMvc;

    private AccountDto accountDto;

    @BeforeEach
    void setUp() {
        accountDto = createTestAccountDto();
    }

    @Test
    void createAccountTest() throws Exception {
        AccountModel accountModel = new AccountModel(accountDto);
        when(accountService.createAccount(accountDto)).thenReturn(accountDto);
        when(accountAssembler.toModel(accountDto)).thenReturn(accountModel);

        mockMvc.perform(post("/account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(EMAIL_JSON_PATH).value(TEST_EMAIL))
                .andExpect(jsonPath(NAME_JSON_PATH).value(TEST_NAME));
    }

    @Test
    void createAccount_incorrectEmailValidationTest() throws Exception {
        accountDto.setEmail("mail");

        mockMvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(MESSAGE_JSON_PATH).value(EMAIL_VALIDATION_MESSAGE))
                .andExpect(jsonPath(ERROR_TYPE_JSON_PATH).value(ErrorType.VALIDATION_ERROR_TYPE.name()));
    }

    @Test
    void createAccount_incorrectPasswordLengthValidationTest() throws Exception {
        accountDto.setPassword("123");
        accountDto.setPasswordRepeat("123");

        mockMvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(MESSAGE_JSON_PATH).value("size must be between 8 and 32"))
                .andExpect(jsonPath(ERROR_TYPE_JSON_PATH).value(ErrorType.VALIDATION_ERROR_TYPE.name()));
    }

    @Test
    void createAccount_notSamePasswordValidationTest() throws Exception {
        accountDto.setPassword("12345678");
        accountDto.setPasswordRepeat("12333567");

        mockMvc.perform(post("/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(MESSAGE_JSON_PATH).value("Passwords are not the same."))
                .andExpect(jsonPath(ERROR_TYPE_JSON_PATH).value(ErrorType.VALIDATION_ERROR_TYPE.name()));
    }

    @Test
    void getAccountTest() throws Exception {
        AccountModel accountModel = new AccountModel(accountDto);

        when(accountService.getAccount(TEST_EMAIL)).thenReturn(accountDto);
        when(accountAssembler.toModel(accountDto)).thenReturn(accountModel);

        mockMvc.perform(get("/account/" + TEST_EMAIL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(EMAIL_JSON_PATH).value(TEST_EMAIL));
    }

    @Test
    void getAccount_incorrectEmailValidationTest() throws Exception {
        final String incorrectEmail = "bad.mail";

        mockMvc.perform(get("/account/" + incorrectEmail))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(MESSAGE_JSON_PATH).value(EMAIL_VALIDATION_MESSAGE))
                .andExpect(jsonPath(ERROR_TYPE_JSON_PATH).value(ErrorType.VALIDATION_ERROR_TYPE.name()));
    }

    @Test
    void updateUserTest() throws Exception {
        AccountModel accountModel = new AccountModel(accountDto);

        when(accountService.updateAccount(TEST_EMAIL, accountDto)).thenReturn(accountDto);
        when(accountAssembler.toModel(accountDto)).thenReturn(accountModel);

        mockMvc.perform(put("/account/" + TEST_EMAIL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(EMAIL_JSON_PATH).value(TEST_EMAIL))
                .andExpect(jsonPath(NAME_JSON_PATH).value(TEST_NAME));
    }

    @Test
    void updateUser_incorrectEmailValidationTest() throws Exception {
        final String badEmail = ")))@email";

        mockMvc.perform(put("/account/" + badEmail)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(MESSAGE_JSON_PATH).value(EMAIL_VALIDATION_MESSAGE))
                .andExpect(jsonPath(ERROR_TYPE_JSON_PATH).value(ErrorType.VALIDATION_ERROR_TYPE.name()));
    }

    @Test
    void updateUser_entityNameLengthValidationTest() throws Exception {
        accountDto.setName("Z");

        mockMvc.perform(put("/account/" + TEST_EMAIL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(MESSAGE_JSON_PATH).value("size must be between 2 and 2147483647"))
                .andExpect(jsonPath(ERROR_TYPE_JSON_PATH).value(ErrorType.VALIDATION_ERROR_TYPE.name()));
    }

    @Test
    void deleteUserTest() throws Exception {
        mockMvc.perform(delete("/account/" + TEST_EMAIL))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteUser_incorrectEmailValidationTest() throws Exception {
        final String badEmail = "saf";
        mockMvc.perform(delete("/account/" + badEmail))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(MESSAGE_JSON_PATH).value(EMAIL_VALIDATION_MESSAGE))
                .andExpect(jsonPath(ERROR_TYPE_JSON_PATH).value(ErrorType.VALIDATION_ERROR_TYPE.name()));
    }

//    @Test
//    void getLoyalty() {
//    }
//
//    @Test
//    void getOrders() {
//    }
}