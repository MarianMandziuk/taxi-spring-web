package com.and.company.service.impl;

import com.and.company.dto.CategoryTranslationDto;
import com.and.company.exception.ServiceException;
import com.and.company.model.CategoryTranslation;
import com.and.company.repository.CategoryTranslationRepository;
import com.and.company.repository.projection.CostView;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static com.and.company.helper.util.TestDataUtil.createTestCategoryTranslationList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CategoryServiceImplTest {

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @Mock
    private CategoryTranslationRepository categoryTranslationRepository;

    @Test
    void getAllCategoriesByLanguageTest() {
        final String langName = "en";
        List<CategoryTranslation> list = createTestCategoryTranslationList();
        when(categoryTranslationRepository.getAllCategoriesByLanguage(langName))
                .thenReturn(list);

        List<CategoryTranslationDto> resultList = categoryService.getAllCategoriesByLanguage(langName);

        assertThat(resultList, allOf(
                is(not(empty())),
                hasSize(list.size())
        ));
    }

    @Test
    void getAllCategoriesByLanguage_emptyListTest() {
        final String langName = "br";
        when(categoryTranslationRepository.getAllCategoriesByLanguage(langName))
                .thenReturn(Arrays.asList());

        List<CategoryTranslationDto> resultList = categoryService.getAllCategoriesByLanguage(langName);

        assertThat(resultList, is(empty()));
        verify(categoryTranslationRepository, times(1)).getAllCategoriesByLanguage(langName);
    }

    @Test
    void getCostPerKmTest() {
        final long languageId = 1;
        final String description = "econom";
        when(categoryTranslationRepository.getCostPerKm(languageId, description))
                .thenReturn(() -> new BigDecimal(2.00));

        BigDecimal costPerKm = categoryService.getCostPerKm(languageId, description);
        assertNotNull(costPerKm);
        assertEquals(2.00, costPerKm.doubleValue());
    }

    @Test
    void getCostPerKm_serviceExceptionTest() {
        final long languageId = 3;
        final String description = "econofm";
        when(categoryTranslationRepository.getCostPerKm(languageId, description))
                .thenReturn(() -> null);
        ServiceException ex = assertThrows(ServiceException.class,
                () -> categoryService.getCostPerKm(languageId, description));
        assertEquals("There is no cost for cars type : " + description, ex.getMessage());
    }

}