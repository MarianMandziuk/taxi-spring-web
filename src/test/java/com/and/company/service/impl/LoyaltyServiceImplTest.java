package com.and.company.service.impl;

import com.and.company.dto.LoyaltyDto;
import com.and.company.exception.LoyaltyNotFoundException;
import com.and.company.model.Loyalty;
import com.and.company.repository.LoyaltyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.and.company.helper.util.TestDataUtil.createTestLoyalty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoyaltyServiceImplTest {

    @InjectMocks
    private LoyaltyServiceImpl loyaltyService;

    @Mock
    private LoyaltyRepository loyaltyRepository;

    @Test
    void getLoyaltyTest() {
        final long id = 1L;
        Loyalty loyalty = createTestLoyalty();
        when(loyaltyRepository.getOne(id)).thenReturn(loyalty);

        LoyaltyDto loyaltyDto = loyaltyService.getLoyalty(id);
        assertThat(loyaltyDto, allOf(
                hasProperty("price", equalTo(loyalty.getPrice())),
                hasProperty("discount", equalTo(loyalty.getDiscount())),
                hasProperty("language", equalTo(loyalty.getLanguage()))
        ));
    }

    @Test
    void getLoyalty_loyaltyNotFoundExceptionTest() {
        final long id = 434L;
        when(loyaltyRepository.getOne(id)).thenReturn(null);

        assertThrows(LoyaltyNotFoundException.class,
                () -> loyaltyService.getLoyalty(id));

    }
}