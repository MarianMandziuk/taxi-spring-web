package com.and.company.service.impl;

import com.and.company.dto.AccountDto;
import com.and.company.exception.AccountNotFoundException;
import com.and.company.model.Account;
import com.and.company.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.and.company.helper.util.TestDataUtil.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @InjectMocks
    private AccountServiceImpl accountService;

    @Mock
    private AccountRepository accountRepository;

    private Account account;

    @BeforeEach
    private void setUp() {
        account = createTestAccount();
    }

    @Test
    void createAccountTest() {
        when(accountRepository.save(any(Account.class))).thenReturn(account);

        AccountDto newAccount = new AccountDto();
        newAccount.setName("User test name");
        newAccount.setEmail("testm@email.com");
        newAccount.setPassword("12345678");

        AccountDto savedAccountDto = accountService.createAccount(newAccount);
        assertNotNull(savedAccountDto);
        assertEquals(account.getName(), savedAccountDto.getName());
        assertEquals(account.getEmail(), savedAccountDto.getEmail());
    }

    @Test
    void getAccountTest() {
        when(accountRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.of(account));

        AccountDto accountDto = accountService.getAccount(TEST_EMAIL);

        assertThat(accountDto, allOf(
                hasProperty("email", equalTo(account.getEmail())),
                hasProperty("name", equalTo(account.getName()))
        ));
        assertEquals(TEST_NAME, accountDto.getName());
    }

    @Test
    void getAccount_AccountNotFoundExceptionTest() {
        when(accountRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.empty());

        assertThrows(AccountNotFoundException.class,
                () -> accountService.getAccount(TEST_EMAIL));
    }

    @Test
    void updateAccountTest() {
        when(accountRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.of(account));
        when(accountRepository.save(any(Account.class))).thenReturn(account);

        AccountDto updatedAccount = new AccountDto();
        updatedAccount.setEmail("my@test.com");
        updatedAccount.setName("test");

        AccountDto updatedAccountDto = accountService.updateAccount(TEST_EMAIL, updatedAccount);
        assertNotNull(updatedAccountDto);
        verify(accountRepository, times(1)).save(any(Account.class));
    }

    @Test
    void updateAccount_AccountNotFoundExceptionTest() {
        when(accountRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.empty());

        AccountDto updatedAccount = new AccountDto();
        updatedAccount.setEmail("my@test.com");
        updatedAccount.setName("test");

        assertThrows(AccountNotFoundException.class,
                () -> accountService.getAccount(TEST_EMAIL));

        verify(accountRepository, times(0)).save(any(Account.class));
        verify(accountRepository, times(0)).delete(any(Account.class));
    }

    @Test
    void deleteAccountTest() {
        when(accountRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.of(account));
        accountService.deleteAccount(TEST_EMAIL);
        verify(accountRepository, times(1)).delete(account);
    }

    @Test
    void deleteAccount_AccountNotFoundExceptionTest() {
        when(accountRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.empty());
        assertThrows(AccountNotFoundException.class,
                () -> accountService.deleteAccount(TEST_EMAIL));
        verify(accountRepository, times(0)).delete(account);
    }
}