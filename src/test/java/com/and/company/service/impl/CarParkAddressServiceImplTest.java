package com.and.company.service.impl;

import com.and.company.dto.CarParkAddressDto;
import com.and.company.exception.CarParkAddressNotFoundException;
import com.and.company.mapper.CarParkAddressMapper;
import com.and.company.model.CarParkAddress;
import com.and.company.repository.CarParkAddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static com.and.company.helper.util.TestDataUtil.createTestCarParkAddress;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarParkAddressServiceImplTest {

    @InjectMocks
    private CarParkAddressServiceImpl carParkAddressService;

    @Mock
    private CarParkAddressRepository carParkAddressRepository;

    private CarParkAddress carParkAddress;

    @BeforeEach
    void setUp() {
        carParkAddress = createTestCarParkAddress();
    }

    @Test
    void updateAddressTest() {
        when(carParkAddressRepository.save(any(CarParkAddress.class))).thenReturn(carParkAddress);

        CarParkAddressDto carParkAddressDtoUpdated = carParkAddressService.updateAddress(
                CarParkAddressMapper.INSTANCE.toCarParkAddressDto(carParkAddress));
        assertNotNull(carParkAddressDtoUpdated);
        verify(carParkAddressRepository, times(1)).save(carParkAddress);
    }

    @Test
    void getCarParkAddressTest() {
        List<CarParkAddress> list = Arrays.asList(carParkAddress);
        when(carParkAddressRepository.findAll()).thenReturn(list);

        CarParkAddressDto carParkAddressDto = carParkAddressService.getCarParkAddress();
        assertThat(carParkAddressDto, allOf(
                hasProperty("address", equalTo(carParkAddress.getAddress())),
                hasProperty("longitude", equalTo(carParkAddress.getLongitude())),
                hasProperty("latitude", equalTo(carParkAddress.getLatitude()))
        ));
    }

    @Test
    void getCarParkAddress_carParkAddressNotFoundExceptionTest() {
        List<CarParkAddress> list = Arrays.asList();
        when(carParkAddressRepository.findAll()).thenReturn(list);

        assertThrows(CarParkAddressNotFoundException.class,
                () -> carParkAddressService.getCarParkAddress());
    }
}