package com.and.company.service.impl;

import com.and.company.dto.OrderTaxiDto;
import com.and.company.mapper.OrderTaxiMapper;
import com.and.company.model.OrderTaxi;
import com.and.company.repository.ClientOrderRepository;
import com.and.company.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.and.company.helper.util.TestDataUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ClientOrderRepository clientOrderRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void createOrderTest() {
        OrderTaxi orderTaxi = createTestOrderTaxi();
        when(orderRepository.save(any(OrderTaxi.class))).thenReturn(orderTaxi);

        OrderTaxiDto orderTaxiDto = orderService.createOrder(
                OrderTaxiMapper.INSTANCE.toOrderTaxiDto(orderTaxi));
        assertNotNull(orderTaxiDto);
        assertNotNull(orderTaxiDto.getPassengerNumber());
        assertEquals(3, orderTaxiDto.getPassengerNumber());
        assertNotNull(orderTaxiDto.getDestinationAddress());
        assertEquals(TEST_DESTINATION_ADDRESS, orderTaxiDto.getDestinationAddress());
        assertNotNull(orderTaxiDto.getPickupAddress());
        assertEquals(TEST_PICKUP_ADDRESS, orderTaxiDto.getPickupAddress());
    }

    @Test
    void getUserOrders() {
    }

    @Test
    void getOrders() {
    }

    @Test
    void getOrdersSortedBy() {
    }

    @Test
    void getOrdersFilteredBetweenDates() {
    }

    @Test
    void getOrdersFilterStartFromDate() {
    }

    @Test
    void getOrdersFilterClientName() {
    }
}