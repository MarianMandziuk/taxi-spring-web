package com.and.company.helper.util;

import com.and.company.dto.AccountDto;
import com.and.company.model.*;
import com.and.company.model.enums.Role;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class TestDataUtil {
    private TestDataUtil() {}

    public static final String TEST_EMAIL = "test_email@mail.com";
    public static final String TEST_NAME = "Test Name";

    public static final String TEST_DESTINATION_ADDRESS = "Main 11b";
    public static final String TEST_PICKUP_ADDRESS = "Main Flowers 224";

    public static Account createTestAccount() {
        Account account = new Account();
        account.setEmail(TEST_EMAIL);
        account.setName(TEST_NAME);
        account.setRole(Role.CUSTOMER);
        return account;
    }

    public static AccountDto createTestAccountDto() {
        AccountDto accountDto = new AccountDto();
        accountDto.setEmail(TEST_EMAIL);
        accountDto.setName(TEST_NAME);
        accountDto.setPassword("12345678");
        accountDto.setPasswordRepeat("12345678");
        return accountDto;
    }

    public static CarParkAddress createTestCarParkAddress() {
        CarParkAddress carParkAddress = new CarParkAddress();
        carParkAddress.setId(1L);
        carParkAddress.setAddress("Test address 5");
        carParkAddress.setLatitude(new BigDecimal("49.100382"));
        carParkAddress.setLongitude(new BigDecimal("24.757618"));
        return carParkAddress;
    }

    public static List<CategoryTranslation> createTestCategoryTranslationList() {
        Language language = createTestLanguageEn();
        CategoryTranslation ct1 = new CategoryTranslation();
        ct1.setId(1L);
        ct1.setDescription("premium");
        ct1.setLanguage(language);
        ct1.setCostPerKM(new BigDecimal(10.00));

        CategoryTranslation ct2 = new CategoryTranslation();
        ct2.setId(2L);
        ct2.setDescription("business");
        ct2.setLanguage(language);
        ct2.setCostPerKM(new BigDecimal(6.00));

        CategoryTranslation ct3 = new CategoryTranslation();
        ct3.setId(2L);
        ct3.setDescription("econom");
        ct3.setLanguage(language);
        ct3.setCostPerKM(new BigDecimal(2.00));
        return Arrays.asList(ct1, ct2, ct3);
    }

    public static Language createTestLanguageEn() {
        Language language = new Language();
        language.setId(1L);
        language.setName("en");
        return language;
    }

    public static Loyalty createTestLoyalty() {
        Loyalty loyalty = new Loyalty();
        loyalty.setId(1L);
        loyalty.setLanguage(createTestLanguageEn());
        loyalty.setDiscount(2);
        loyalty.setPrice(BigDecimal.valueOf(500.00));
        return loyalty;
    }

    public static OrderTaxi createTestOrderTaxi() {
        OrderTaxi orderTaxi = new OrderTaxi();
        orderTaxi.setId(1L);
        orderTaxi.setPassengerNumber(3);
        orderTaxi.setDestinationAddress(TEST_DESTINATION_ADDRESS);
        orderTaxi.setPickupAddress(TEST_PICKUP_ADDRESS);
        orderTaxi.setCost(BigDecimal.valueOf(32.00));
        orderTaxi.setLatitudeDestination(BigDecimal.valueOf(49.534756));
        orderTaxi.setLongitudeDestination(BigDecimal.valueOf( 30.988777));
        orderTaxi.setLatitudePickup(BigDecimal.valueOf(49.747255));
        orderTaxi.setLongitudePickup(BigDecimal.valueOf(31.451977));
        orderTaxi.setAccount(createTestAccount());
        orderTaxi.setLanguage(createTestLanguageEn());
        orderTaxi.setCarList(createTestCarList());
        return orderTaxi;
    }

    public static List<Car> createTestCarList() {
        Car car = new Car();
        car.setCapacity(3);
        car.setModelName("audi");
        return Arrays.asList(car);
    }
}
