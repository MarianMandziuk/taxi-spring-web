package com.and.company.service;

import com.and.company.dto.AccountDto;

public interface AccountService {
    AccountDto createAccount(AccountDto accountDto);

    AccountDto getAccount(String email);

    AccountDto updateAccount(String email, AccountDto accountDto);

    void deleteAccount(String email);
}
