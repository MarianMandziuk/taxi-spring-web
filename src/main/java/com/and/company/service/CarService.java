package com.and.company.service;

import com.and.company.dto.CreateOrderDto;
import com.and.company.dto.OrderCompletedDto;

public interface CarService {
    OrderCompletedDto getAvailableCarByCapacityAndStatus(CreateOrderDto createOrderDto);
    OrderCompletedDto getSeveralCarsByCapacity(CreateOrderDto createOrderDto);
}
