package com.and.company.service;

import com.and.company.dto.LoyaltyDto;

public interface LoyaltyService {
    LoyaltyDto getLoyalty(Long id);
}
