package com.and.company.service;

import com.and.company.dto.CarParkAddressDto;

public interface CarParkAddressService {
    CarParkAddressDto updateAddress(CarParkAddressDto addressDto);

    CarParkAddressDto getCarParkAddress();
}
