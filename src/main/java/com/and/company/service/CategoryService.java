package com.and.company.service;

import com.and.company.dto.CategoryTranslationDto;

import java.math.BigDecimal;
import java.util.List;

public interface CategoryService {
    List<CategoryTranslationDto> getAllCategoriesByLanguage(String languageName);
    BigDecimal getCostPerKm(Long languageId, String description);
}
