package com.and.company.service;

import com.and.company.dto.ClientOrderDto;
import com.and.company.dto.OrderTaxiDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderService {
    OrderTaxiDto createOrder(OrderTaxiDto orderTaxiDto);
    List<OrderTaxiDto> getUserOrders(long id);

    Page<ClientOrderDto> getOrders(Pageable pageable);
    Page<ClientOrderDto> getOrdersSortedBy(Pageable pageable);

    Page<ClientOrderDto> getOrdersFilteredBetweenDates(LocalDateTime fromDate, LocalDateTime toDate,
                                                       Pageable pageable);
    Page<ClientOrderDto> getOrdersFilterStartFromDate(LocalDateTime fromDate,
                                                      Pageable pageable);
    Page<ClientOrderDto> getOrdersFilterClientName(String name, Pageable pageable);
}
