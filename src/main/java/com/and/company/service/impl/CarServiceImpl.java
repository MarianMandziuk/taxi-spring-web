package com.and.company.service.impl;

import com.and.company.dto.CreateOrderDto;
import com.and.company.dto.OrderCompletedDto;
import com.and.company.exception.CarParkAddressNotFoundException;
import com.and.company.exception.LoyaltyNotFoundException;
import com.and.company.exception.ServiceException;
import com.and.company.mapper.OrderTaxiMapper;
import com.and.company.model.Car;
import com.and.company.model.CarParkAddress;
import com.and.company.model.Loyalty;
import com.and.company.model.OrderTaxi;
import com.and.company.model.enums.Status;
import com.and.company.repository.*;
import com.and.company.repository.projection.CostView;
import com.and.company.service.CarService;
import com.and.company.util.Distance;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.and.company.util.Distance.minutesSpend;

@Slf4j
@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final LanguageRepository languageRepository;
    private final CategoryTranslationRepository categoryTranslationRepository;
    private final LoyaltyRepository loyaltyRepository;
    private final OrderRepository orderRepository;
    private final CarParkAddressRepository carParkAddressRepository;

    @Transactional
    @Override
    public OrderCompletedDto getAvailableCarByCapacityAndStatus(CreateOrderDto createOrderDto) {
        log.info("Getting car by order: {}", createOrderDto);
        long languageId = createOrderDto.getLanguageId();
        Car car = carRepository.findFirstCarByCapacityAndStatus(
                languageId,
                createOrderDto.getPassengerNumber(),
                Status.AVAILABLE.ordinal(),
                createOrderDto.getCategory());
        String langName = languageRepository.findById(createOrderDto.getLanguageId())
                .orElseThrow(() -> new ServiceException("Language: " + createOrderDto.getLanguageId() + " is not found."))
                .getName();
        if (car == null) {
            return new OrderCompletedDto("There is no suitable car, please try other options",
                    0, "", new BigDecimal(0), langName);
        }
        OrderTaxi orderTaxi = OrderTaxiMapper.INSTANCE.toOrderTaxiDto(createOrderDto);
        BigDecimal cost = getCostForOneCar(createOrderDto, languageId);
        orderTaxi.setCost(cost);
        orderTaxi.setCarList(Arrays.asList(car));
        orderTaxi = orderRepository.save(orderTaxi);

        updateCarStatus(car, Status.BUSY);

        CarParkAddress carParkAddress = getParkAddress();

        int waitKm = Distance.getDistanceFromLatLonInKm(createOrderDto.getLatitudePickup().doubleValue(),
                createOrderDto.getLongitudePickup().doubleValue(),
                carParkAddress.getLatitude().doubleValue(),
                carParkAddress.getLongitude().doubleValue());
        int minutes = minutesSpend(waitKm);

        return new OrderCompletedDto(car.getModelName(),
                minutes, orderTaxi.getPickupAddress(), cost, langName);

    }

    @Transactional
    @Override
    public OrderCompletedDto getSeveralCarsByCapacity(CreateOrderDto createOrderDto) {
        log.info("<multiple>Get available cars for order: {}.", createOrderDto);
        long languageId = createOrderDto.getLanguageId();
        List<Car> cars = carRepository.getSeveralCarsByCapacity(
                languageId,
                createOrderDto.getPassengerNumber(),
                Status.AVAILABLE.ordinal(),
                createOrderDto.getCategory());
        String langName = languageRepository.findById(createOrderDto.getLanguageId())
                .orElseThrow(() -> new ServiceException("Language: " + createOrderDto.getLanguageId() + " is not found."))
                .getName();
        if (cars == null || cars.isEmpty()) {
            return new OrderCompletedDto("There are no suitable cars, please try other options",
                    0, "", new BigDecimal(0), langName);
        }
        OrderTaxi orderTaxi = OrderTaxiMapper.INSTANCE.toOrderTaxiDto(createOrderDto);
        BigDecimal cost = getCostForOneCar(createOrderDto, languageId);
        cost = cost.multiply(new BigDecimal(cars.size()));
        orderTaxi.setCost(cost);
        orderTaxi = orderRepository.save(orderTaxi);
        orderTaxi.setCarList(cars);
        for (Car car : cars) {
            updateCarStatus(car, Status.BUSY);
        }

        CarParkAddress carParkAddress = getParkAddress();

        int waitKm = Distance.getDistanceFromLatLonInKm(createOrderDto.getLatitudePickup().doubleValue(),
                createOrderDto.getLongitudePickup().doubleValue(),
                carParkAddress.getLatitude().doubleValue(),
                carParkAddress.getLongitude().doubleValue());
        int minutes = minutesSpend(waitKm);

        return new OrderCompletedDto(cars.stream().map(Car::getModelName).collect(Collectors.joining(", ")),
                minutes, orderTaxi.getPickupAddress(), cost, langName);
    }

    private BigDecimal getCostForOneCar(CreateOrderDto createOrderDto, Long languageId) {
        log.info("Getting cost for one car for {} and lang: {}", createOrderDto, languageId);
        CostView costView = categoryTranslationRepository.getCostPerKm(languageId, createOrderDto.getCategory());
        if (costView == null || costView.getCostPerKM() == null) {
            throw new ServiceException("There is no cost for cars type : " + createOrderDto.getCategory());
        }
        BigDecimal costByKm = costView.getCostPerKM();

        log.info("Calculating distance between two points");

        int km = Distance.getDistanceFromLatLonInKm(createOrderDto.getLatitudeDestination().doubleValue(),
                createOrderDto.getLongitudeDestination().doubleValue(),
                createOrderDto.getLatitudePickup().doubleValue(),
                createOrderDto.getLongitudePickup().doubleValue());
        Loyalty loyalty = loyaltyRepository.getOne(createOrderDto.getAccountId());
        if (loyalty == null) {
            throw new LoyaltyNotFoundException();
        }

        BigDecimal cost;
        log.info("Calculating loyalty");
        if (loyalty != null && loyalty.getDiscount() > 0) {
            cost = calculateLoyaltyCost(km, loyalty.getDiscount(), costByKm);
        } else {
            cost = calculateCost(km, costByKm);
        }

        return cost;
    }

    private CarParkAddress getParkAddress() {
        log.info("Getting car park address in from database");
        List<CarParkAddress> addresses = carParkAddressRepository.findAll();
        if (addresses.isEmpty()) {
            throw new CarParkAddressNotFoundException();
        }
        return addresses.get(0);
    }

    private void updateCarStatus(Car car, Status status) {
        log.info("Updating car's status in database: {}, {}", car, status);
        carRepository.updateCarStatus(car.getId(), status);
    }

    private static BigDecimal calculateCost(int km, BigDecimal costByKm) {
        return costByKm.multiply(new BigDecimal(km));
    }

    private static BigDecimal calculateLoyaltyCost(int km, int loyalty, BigDecimal costByKm) {
        BigDecimal result = costByKm.multiply(new BigDecimal(km));
        BigDecimal left = result.multiply(new BigDecimal(loyalty))
                .divide(new BigDecimal(100));
        return result.subtract(left);
    }
}
