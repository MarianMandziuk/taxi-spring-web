package com.and.company.service.impl;

import com.and.company.dto.AccountDto;
import com.and.company.exception.AccountNotFoundException;
import com.and.company.mapper.AccountMapper;
import com.and.company.model.Account;
import com.and.company.repository.AccountRepository;
import com.and.company.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Override
    public AccountDto createAccount(AccountDto accountDto) {
        log.info("Creating account in database: {}", accountDto);
        Account account = AccountMapper.INSTANCE.toAccount(accountDto);
        account = accountRepository.save(account);
        return AccountMapper.INSTANCE.toAccountDto(account);
    }

    @Override
    public AccountDto getAccount(String email) {
        log.info("Cetting account from database by email {}", email);
        Account acc = accountRepository.findByEmail(email)
                .orElseThrow(AccountNotFoundException::new);
        return AccountMapper.INSTANCE.toAccountDto(acc);
    }

    @Override
    public AccountDto updateAccount(String email, AccountDto accountDto) {
        log.info("Updating account in database {}", accountDto);
        Account account = AccountMapper.INSTANCE.toAccount(accountDto);
        Account accountFromDb = accountRepository.findByEmail(email)
                .orElseThrow(AccountNotFoundException::new);
        accountRepository.delete(accountFromDb);
        account = accountRepository.save(account);
        return AccountMapper.INSTANCE.toAccountDto(account);
    }

    @Override
    public void deleteAccount(String email) {
        log.info("Deleting account from database: {}", email);
        Account accountFromDb = accountRepository.findByEmail(email)
                .orElseThrow(AccountNotFoundException::new);
        accountRepository.delete(accountFromDb);
    }
}
