package com.and.company.service.impl;

import com.and.company.dto.CategoryTranslationDto;
import com.and.company.exception.ServiceException;
import com.and.company.mapper.CategoryTranslationMapper;
import com.and.company.model.CategoryTranslation;
import com.and.company.repository.CategoryTranslationRepository;
import com.and.company.repository.projection.CostView;
import com.and.company.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryTranslationRepository categoryTranslationRepository;

    @Override
    public List<CategoryTranslationDto> getAllCategoriesByLanguage(String languageName) {
        log.info("Getting categories from database by language {}", languageName);
        List<CategoryTranslation> categories = categoryTranslationRepository.getAllCategoriesByLanguage(languageName);
        return categories.stream()
                    .map(CategoryTranslationMapper.INSTANCE::toCategoryTranslationDto)
                    .collect(Collectors.toList());
    }

    @Override
    public BigDecimal getCostPerKm(Long languageId, String description) {
        log.info("Getting cost per km for car status {} ", description);
        CostView cost = categoryTranslationRepository.getCostPerKm(languageId, description);
        if (cost == null || cost.getCostPerKM() == null) {
            throw new ServiceException("There is no cost for cars type : " + description);
        }
        return cost.getCostPerKM();
    }
}
