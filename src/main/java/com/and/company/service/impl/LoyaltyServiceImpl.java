package com.and.company.service.impl;

import com.and.company.dto.LoyaltyDto;
import com.and.company.exception.LoyaltyNotFoundException;
import com.and.company.mapper.LoyaltyMapper;
import com.and.company.model.Loyalty;
import com.and.company.repository.LoyaltyRepository;
import com.and.company.service.LoyaltyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoyaltyServiceImpl implements LoyaltyService {

    private final LoyaltyRepository loyaltyRepository;

    @Override
    public LoyaltyDto getLoyalty(Long id) {
        log.info("Getting loyalty by client id {}", id);
        Loyalty loyalty = loyaltyRepository.getOne(id);
        if (loyalty == null) {
            throw new LoyaltyNotFoundException();
        }
        return LoyaltyMapper.INSTANCE.toLoyaltyDto(loyalty);
    }
}
