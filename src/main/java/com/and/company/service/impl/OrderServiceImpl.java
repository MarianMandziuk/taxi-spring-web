package com.and.company.service.impl;

import com.and.company.dto.ClientOrderDto;
import com.and.company.dto.OrderTaxiDto;
import com.and.company.mapper.ClientOrderMapper;
import com.and.company.mapper.OrderTaxiMapper;
import com.and.company.model.OrderTaxi;
import com.and.company.repository.ClientOrderRepository;
import com.and.company.repository.OrderRepository;
import com.and.company.repository.projection.ClientOrderView;
import com.and.company.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.ap.internal.util.Collections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ClientOrderRepository clientOrderRepository;

    @Override
    public OrderTaxiDto createOrder(OrderTaxiDto orderTaxiDto) {
        log.info("Creating order taxi in database: {}", orderTaxiDto);
        OrderTaxi orderTaxi = OrderTaxiMapper.INSTANCE.toOrderTaxi(orderTaxiDto);
        orderTaxi = orderRepository.save(orderTaxi);
        return OrderTaxiMapper.INSTANCE.toOrderTaxiDto(orderTaxi);
    }

    @Override
    public List<OrderTaxiDto> getUserOrders(long id) {
        log.info("Getting user's orders from database by id: {}", id);
        List<OrderTaxi> listOrders = orderRepository.findAllById(Collections.asSet(id));
        return listOrders.stream()
                    .map(OrderTaxiMapper.INSTANCE::toOrderTaxiDto)
                    .collect(Collectors.toList());
    }

    @Override
    public Page<ClientOrderDto> getOrders(Pageable pageable) {
        log.info("Getting orders from database, with pagination: {} .", pageable);
        Page<ClientOrderView> pageView = clientOrderRepository.findAllClientOrders(pageable);
        return pageView.map(ClientOrderMapper.INSTANCE::toClientOrderDto);
    }

    @Override
    public Page<ClientOrderDto> getOrdersSortedBy(Pageable pageable) {
        log.info("Getting orders from database sorted with {} ", pageable);
        Page<ClientOrderView> pageView = clientOrderRepository.findAllClientOrders(pageable);
        return pageView.map(ClientOrderMapper.INSTANCE::toClientOrderDto);
    }

    @Override
    public Page<ClientOrderDto> getOrdersFilteredBetweenDates(LocalDateTime fromDate, LocalDateTime toDate,
                                                              Pageable pageable) {
        log.info("Getting orders from database between dates {} and {}", fromDate, toDate);
        Page<ClientOrderView> pageView = clientOrderRepository.getOrdersFilteredBetweenDates(fromDate, toDate, pageable);
        return pageView.map(ClientOrderMapper.INSTANCE::toClientOrderDto);
    }

    @Override
    public Page<ClientOrderDto> getOrdersFilterStartFromDate(LocalDateTime fromDate, Pageable pageable) {
        log.info("Getting orders from database start from date {}", fromDate);
        Page<ClientOrderView> pageView = clientOrderRepository.getOrdersFilterStartFromDate(fromDate, pageable);
        return pageView.map(ClientOrderMapper.INSTANCE::toClientOrderDto);
    }

    @Override
    public Page<ClientOrderDto> getOrdersFilterClientName(String name, Pageable pageable) {
        log.info("Getting orders from database by client name {}", name);
        Page<ClientOrderView> pageView = clientOrderRepository.getOrdersFilterClientName(name, pageable);
        return pageView.map(ClientOrderMapper.INSTANCE::toClientOrderDto);
    }
}
