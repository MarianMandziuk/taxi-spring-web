package com.and.company.service.impl;

import com.and.company.dto.CarParkAddressDto;
import com.and.company.exception.CarParkAddressNotFoundException;
import com.and.company.mapper.CarParkAddressMapper;
import com.and.company.model.CarParkAddress;
import com.and.company.repository.CarParkAddressRepository;
import com.and.company.service.CarParkAddressService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CarParkAddressServiceImpl implements CarParkAddressService {

    private final CarParkAddressRepository carParkAddressRepository;

    @Override
    public CarParkAddressDto updateAddress(CarParkAddressDto addressDto) {
        log.info("Updating car park address in database: {}", addressDto);
        CarParkAddress address = CarParkAddressMapper.INSTANCE.toCarParkAddress(addressDto);
        address = carParkAddressRepository.save(address);
        return CarParkAddressMapper.INSTANCE.toCarParkAddressDto(address);
    }

    @Override
    public CarParkAddressDto getCarParkAddress() {
        log.info("Getting car park address in from database");
        List<CarParkAddress> addresses = carParkAddressRepository.findAll();
        if (addresses.isEmpty()) {
            throw new CarParkAddressNotFoundException();
        }
        return CarParkAddressMapper.INSTANCE.toCarParkAddressDto(addresses.get(0));
    }
}
