package com.and.company.validators;

import com.and.company.dto.AccountDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidation implements ConstraintValidator<SamePassword, AccountDto> {

    @Override
    public boolean isValid(AccountDto value, ConstraintValidatorContext context) {
        return value.getPassword().equals(value.getPasswordRepeat());
    }
}
