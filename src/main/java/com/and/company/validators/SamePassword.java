package com.and.company.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordValidation.class)
public @interface SamePassword {

    String message() default "Passwords are not the same.";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
