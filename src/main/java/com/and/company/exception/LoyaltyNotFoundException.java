package com.and.company.exception;

import com.and.company.model.enums.ErrorType;

public class LoyaltyNotFoundException extends ServiceException {
    private static final String DEFAULT_MESSAGE = "Client has not any loyalty yet!";

    public LoyaltyNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public LoyaltyNotFoundException(String message) {
        super(message);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DATABASE_ERROR_TYPE;
    }
}
