package com.and.company.exception;

import com.and.company.model.enums.ErrorType;

public class CarParkAddressNotFoundException extends ServiceException {
    private static final String DEFAULT_MESSAGE = "Car park addres is not found!";

    public CarParkAddressNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public CarParkAddressNotFoundException(String message) {
        super(message);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DATABASE_ERROR_TYPE;
    }
}
