package com.and.company.mapper;

import com.and.company.dto.CreateOrderDto;
import com.and.company.dto.OrderTaxiDto;
import com.and.company.model.OrderTaxi;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderTaxiMapper {
    OrderTaxiMapper INSTANCE = Mappers.getMapper(OrderTaxiMapper.class);

    OrderTaxiDto toOrderTaxiDto(OrderTaxi orderTaxi);

    OrderTaxi toOrderTaxi(OrderTaxiDto orderTaxiDto);

    OrderTaxi toOrderTaxiDto(CreateOrderDto createOrderDto);
}
