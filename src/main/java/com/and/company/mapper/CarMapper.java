package com.and.company.mapper;

import com.and.company.dto.CarDto;
import com.and.company.model.Car;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarMapper {
    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    CarDto toCarDto(Car car);

    Car toCar(CarDto carDto);
}
