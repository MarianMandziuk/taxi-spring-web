package com.and.company.mapper;

import com.and.company.dto.ClientOrderDto;
import com.and.company.repository.projection.ClientOrderView;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClientOrderMapper {
    ClientOrderMapper INSTANCE = Mappers.getMapper(ClientOrderMapper.class);

    ClientOrderDto toClientOrderDto(ClientOrderView view);
}
