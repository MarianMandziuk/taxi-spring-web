package com.and.company.mapper;

import com.and.company.dto.LoyaltyDto;
import com.and.company.model.Loyalty;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LoyaltyMapper {
    LoyaltyMapper INSTANCE = Mappers.getMapper(LoyaltyMapper.class);

    LoyaltyDto toLoyaltyDto(Loyalty loyalty);

    Loyalty toLoyalty(LoyaltyDto loyaltyDto);
}
