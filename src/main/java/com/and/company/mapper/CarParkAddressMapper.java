package com.and.company.mapper;

import com.and.company.dto.CarParkAddressDto;
import com.and.company.model.CarParkAddress;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarParkAddressMapper {
    CarParkAddressMapper INSTANCE = Mappers.getMapper(CarParkAddressMapper.class);

    CarParkAddressDto toCarParkAddressDto(CarParkAddress carParkAddress);

    CarParkAddress toCarParkAddress(CarParkAddressDto carParkAddressDto);
}
