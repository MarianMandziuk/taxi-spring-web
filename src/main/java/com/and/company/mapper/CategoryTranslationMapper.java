package com.and.company.mapper;

import com.and.company.dto.CategoryTranslationDto;
import com.and.company.model.CategoryTranslation;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoryTranslationMapper {
    CategoryTranslationMapper INSTANCE = Mappers.getMapper(CategoryTranslationMapper.class);

    CategoryTranslationDto toCategoryTranslationDto(CategoryTranslation categoryTranslation);

    CategoryTranslation toCategoryTranslation(CategoryTranslationDto CategoryTranslationDto);
}
