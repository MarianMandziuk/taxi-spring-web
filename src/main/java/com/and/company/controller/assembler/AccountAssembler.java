package com.and.company.controller.assembler;

import com.and.company.controller.AccountController;
import com.and.company.controller.model.AccountModel;
import com.and.company.dto.AccountDto;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class AccountAssembler extends RepresentationModelAssemblerSupport<AccountDto, AccountModel> {
    public AccountAssembler() {
        super(AccountController.class, AccountModel.class);
    }

    @Override
    public AccountModel toModel(AccountDto entity) {
        AccountModel accountModel = new AccountModel(entity);
        Link getOrders = linkTo(methodOn(AccountController.class).getOrders(entity.getId())).withRel("get_orders");
        Link getLoyalty = linkTo(methodOn(AccountController.class).getLoyalty(entity.getId())).withRel("get_loyalty");
        accountModel.add(getOrders, getLoyalty);

        return accountModel;
    }
}
