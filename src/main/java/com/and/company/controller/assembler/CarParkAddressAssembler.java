package com.and.company.controller.assembler;

import com.and.company.controller.CarParkAddressController;
import com.and.company.controller.model.CarParkAddressModel;
import com.and.company.dto.CarParkAddressDto;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CarParkAddressAssembler extends RepresentationModelAssemblerSupport<CarParkAddressDto, CarParkAddressModel> {
    public CarParkAddressAssembler() {
        super(CarParkAddressController.class, CarParkAddressModel.class);
    }

    @Override
    public CarParkAddressModel toModel(CarParkAddressDto entity) {
        CarParkAddressModel carParkAddressModel = new CarParkAddressModel(entity);
        Link getAddress = linkTo(methodOn(CarParkAddressController.class).getCarParkAddress()).withRel("get_address");
        Link updateAddress = linkTo(methodOn(CarParkAddressController.class).updateAddress(entity)).withRel("update_address");
        carParkAddressModel.add(getAddress, updateAddress);
        return carParkAddressModel;
    }
}
