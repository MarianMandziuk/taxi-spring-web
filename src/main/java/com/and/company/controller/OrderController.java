package com.and.company.controller;

import com.and.company.dto.ClientOrderDto;
import com.and.company.service.OrderService;
import com.and.company.validators.ValuesAllowed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Slf4j
@Validated
@RestController
@RequestMapping("/get_order")
@RequiredArgsConstructor
@Api("Order info api")
public class OrderController {

    private final OrderService orderService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{page}/{items}/all")
    @ApiOperation("Get orders")
    public Page<ClientOrderDto> getOrders(@PathVariable @Min(0) int page,
                                          @PathVariable @Min(1) int items) {
        log.info("Get orders on page {} with items {}.", page, items);
        Pageable pageable = PageRequest.of(page, items);
        return orderService.getOrders(pageable);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{page}/{items}/{field}/descending")
    @ApiOperation("Get sorted orders by time")
    public Page<ClientOrderDto> getOrdersSortedByDescending(@PathVariable @Min(0) int page,
                                                  @PathVariable @Min(1) int items,
                                                  @PathVariable @ValuesAllowed(values = { "createTime", "cost", "name"})
                                                              String field) {
        log.info("Get orders sorted by {} on page {} with items {}", field, page, items);
        Pageable pageable = PageRequest.of(page, items, Sort.by(field).descending());
        return orderService.getOrdersSortedBy(pageable);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{page}/{items}/{field}/ascending")
    @ApiOperation("Get sorted orders by time")
    public Page<ClientOrderDto> getOrdersSortedByAscending(@PathVariable @Min(0) int page,
                                                  @PathVariable @Min(1) int items,
                                                  @PathVariable @ValuesAllowed(values = { "createTime", "cost", "name"})
                                                          String field) {
        log.info("Get orders sorted by {} on page {} with items {}", field, page, items);
        Pageable pageable = PageRequest.of(page, items, Sort.by(field).ascending());
        return orderService.getOrdersSortedBy(pageable);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{fromDate}/{toDate}/{page}/{items}/between_dates")
    @ApiOperation("Get orders between dates")
    public Page<ClientOrderDto> getOrdersFilteredBetweenDates(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE,
                                                                pattern= "yyyy-MM-dd") LocalDate fromDate,
                                                              @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE,
                                                                pattern= "yyyy-MM-dd") LocalDate toDate,
                                                              @PathVariable @Min(0) int page,
                                                              @PathVariable @Min(1) int items) {
        log.info("Get orders between dates {} and {}, from {} to {}.", fromDate, toDate, page, items);
        Pageable pageable = PageRequest.of(page, items);
        LocalDateTime fromDateTime = fromDate.atStartOfDay();
        LocalDateTime toDateTime = toDate.atStartOfDay();
        return orderService.getOrdersFilteredBetweenDates(fromDateTime, toDateTime, pageable);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{fromDate}/{page}/{items}/from_date")
    @ApiOperation("Get orders from date")
    public Page<ClientOrderDto> getOrdersFilterStartFromDate(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE,
                                                                pattern= "yyyy-MM-dd") LocalDate fromDate,
                                                             @PathVariable @Min(0) int page,
                                                             @PathVariable @Min(1) int items) {
        log.info("Get orders strart from date {}", fromDate);
        Pageable pageable = PageRequest.of(page, items);
        LocalDateTime fromDateTime = fromDate.atStartOfDay();
        return orderService.getOrdersFilterStartFromDate(fromDateTime, pageable);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{name}/{page}/{items}/client_name")
    @ApiOperation("Filter orders by client name")
    public Page<ClientOrderDto> getOrdersFilterClientName(@PathVariable String name,
                                                          @PathVariable @Min(0) int page,
                                                          @PathVariable @Min(1) int items) {
        log.info("Get orders filtered by client name: {}.", name);
        Pageable pageable = PageRequest.of(page, items);
        return orderService.getOrdersFilterClientName(name, pageable);
    }
}
