package com.and.company.controller.model;

import com.and.company.dto.CarParkAddressDto;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class CarParkAddressModel extends RepresentationModel<CarParkAddressModel> {
    @JsonUnwrapped
    private CarParkAddressDto carParkAddressDto;
}
