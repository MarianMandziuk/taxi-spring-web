package com.and.company.controller;

import com.and.company.controller.assembler.CarParkAddressAssembler;
import com.and.company.controller.model.CarParkAddressModel;
import com.and.company.dto.CarParkAddressDto;
import com.and.company.service.CarParkAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/car_park_address")
@RequiredArgsConstructor
@Api("Car park address management api")
public class CarParkAddressController {

    private final CarParkAddressService carParkAddressService;
    private final CarParkAddressAssembler carParkAddressAssembler;

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/update")
    @ApiOperation("Update car park address")
    public CarParkAddressModel updateAddress(@Valid @RequestBody CarParkAddressDto addressDto) {
        log.info("Update car par address {}", addressDto);
        CarParkAddressDto carParkAddressDto = carParkAddressService.updateAddress(addressDto);
        return carParkAddressAssembler.toModel(carParkAddressDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    @ApiOperation("Get park address")
    public CarParkAddressModel getCarParkAddress() {
        log.info("Get car park address");
        CarParkAddressDto carParkAddressDto = carParkAddressService.getCarParkAddress();
        return carParkAddressAssembler.toModel(carParkAddressDto);
    }
}
