package com.and.company.controller;

import com.and.company.controller.assembler.AccountAssembler;
import com.and.company.controller.model.AccountModel;
import com.and.company.dto.AccountDto;
import com.and.company.dto.LoyaltyDto;
import com.and.company.dto.OrderTaxiDto;
import com.and.company.dto.group.Mandatory;
import com.and.company.dto.group.Secondary;
import com.and.company.service.AccountService;
import com.and.company.service.LoyaltyService;
import com.and.company.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
@Validated
@Api("Represent api of operation and information to account")
public class AccountController {

    private final AccountService accountService;
    private final LoyaltyService loyaltyService;
    private final OrderService orderService;
    private final AccountAssembler accountAssembler;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    @ApiOperation("Create account for client")
    public AccountModel createAccount(@Validated(Mandatory.class) @RequestBody AccountDto accountDto) {
        log.info("Create account {}", accountDto);
        AccountDto account = accountService.createAccount(accountDto);
        return accountAssembler.toModel(account);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{email}")
    @ApiOperation("Retrieving account by email")
    public AccountModel getAccount(@PathVariable @Email String email) {
        log.info("Get account by email: {}", email);
        AccountDto account = accountService.getAccount(email);
        return accountAssembler.toModel(account);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/{email}")
    public AccountModel updateUser(@PathVariable @Email String email, @Validated(Secondary.class) @RequestBody AccountDto accountDto) {
        log.info("Update account's email {}", email);
        AccountDto account = accountService.updateAccount(email, accountDto);
        return accountAssembler.toModel(account);
    }

    @DeleteMapping("/{email}")
    public ResponseEntity<Void> deleteUser(@PathVariable @Email String email) {
        log.info("Delete account {}", email);
        accountService.deleteAccount(email);
        return ResponseEntity.noContent().build();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{id}/loyalty")
    @ApiOperation("Retrieving account's loyalty by id")
    public LoyaltyDto getLoyalty(@PathVariable Long id) {
        log.info("Get loyalty by client id {}", id);
        return loyaltyService.getLoyalty(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{id}/orders")
    @ApiOperation("Retrieving account's orders by id")
    public List<OrderTaxiDto> getOrders(@PathVariable Long id) {
        log.info("Get orders by client id {}", id);
        return orderService.getUserOrders(id);
    }
}
