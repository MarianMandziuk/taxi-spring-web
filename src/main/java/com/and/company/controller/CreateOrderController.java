package com.and.company.controller;

import com.and.company.dto.*;
import com.and.company.mapper.OrderTaxiMapper;
import com.and.company.model.enums.Status;
import com.and.company.service.*;
import com.and.company.util.Distance;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.and.company.util.Distance.minutesSpend;

@Slf4j
@RestController
@RequestMapping("/create_order")
@RequiredArgsConstructor
@Api("Create order api")
public class CreateOrderController {

    private final CarService carService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/single")
    @ApiOperation("Get one available car")
    public OrderCompletedDto getAvailableCar(@Valid @RequestBody CreateOrderDto createOrderDto) {
        log.info("<single>Get available car for order: {}.", createOrderDto);
        return carService.getAvailableCarByCapacityAndStatus(createOrderDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/multiple")
    @ApiOperation("Get multiple available cars.")
    public OrderCompletedDto getSeveralCars(@Valid @RequestBody CreateOrderDto createOrderDto) {
        return carService.getSeveralCarsByCapacity(createOrderDto);
    }

}
