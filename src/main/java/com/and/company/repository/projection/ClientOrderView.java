package com.and.company.repository.projection;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface ClientOrderView {
    String getName();
    Integer getPassengerNumber();
    String getDestinationAddress();
    String getPickupAddress();
    BigDecimal getCost();
    LocalDateTime getCreateTime();
    String getLanguageName();
}
