package com.and.company.repository.projection;

import java.math.BigDecimal;

public interface CostView {
    BigDecimal getCostPerKM();
}
