package com.and.company.repository;

import com.and.company.model.OrderTaxi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<OrderTaxi, Long> {
}
