package com.and.company.repository;

import com.and.company.model.Car;
import com.and.company.model.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Car findFirstCarByCapacityAndStatus(@Param("languageId") Long languageId,
                                        @Param("capacity") Integer capacity,
                                        @Param("status") int status,
                                        @Param("category") String category);

    List<Car> getSeveralCarsByCapacity(@Param("languageId") Long languageId,
                                       @Param("capacity") Integer capacity,
                                       @Param("status") int status,
                                       @Param("category") String category);

    @Modifying
    void updateCarStatus(@Param("carId") Long carId, @Param("status") Status status);
}
