package com.and.company.repository;

import com.and.company.model.CarParkAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkAddressRepository extends JpaRepository<CarParkAddress, Long> {

}
