package com.and.company.repository;

import com.and.company.model.Account;
import com.and.company.repository.projection.ClientOrderView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface ClientOrderRepository extends JpaRepository<Account, Long> {

    @Query("SELECT a.name as name, ot.passengerNumber as passengerNumber, " +
            "ot.destinationAddress as destinationAddress,\n" +
            "ot.pickupAddress as pickupAddress, " +
            "ot.cost as cost, ot.createTime as createTime, " +
            "l.name AS languageName\n" +
            "FROM Account a, OrderTaxi ot, Language l\n" +
            "WHERE a.id = ot.account\n" +
            "AND l.id = ot.language\n")
    Page<ClientOrderView> findAllClientOrders(Pageable pageable);

    @Query("SELECT a.name as name, ot.passengerNumber as passengerNumber, " +
            "ot.destinationAddress as  destinationAddress, " +
            "ot.pickupAddress as pickupAddress, ot.cost as cost, ot.createTime as createTime, " +
            "l.name as languageName " +
            "FROM OrderTaxi ot " +
            "INNER JOIN ot.account as a " +
            "INNER JOIN ot.language as l " +
            "WHERE ot.createTime BETWEEN ?1 AND ?2")
    Page<ClientOrderView> getOrdersFilteredBetweenDates(LocalDateTime fromDate, LocalDateTime toDate,
                                                        Pageable pageable);

    @Query("SELECT a.name as name, ot.passengerNumber as passengerNumber, " +
            "ot.destinationAddress as  destinationAddress, " +
            "ot.pickupAddress as pickupAddress, ot.cost as cost, ot.createTime as createTime, " +
            "l.name as languageName " +
            "FROM OrderTaxi ot " +
            "INNER JOIN ot.account as a " +
            "INNER JOIN ot.language as l " +
            "WHERE ot.createTime >= ?1")
    Page<ClientOrderView> getOrdersFilterStartFromDate(LocalDateTime fromDate,
                                                       Pageable pageable);

    @Query("SELECT a.name as name, ot.passengerNumber as passengerNumber, " +
            "ot.destinationAddress as  destinationAddress, " +
            "ot.pickupAddress as pickupAddress, ot.cost as cost, ot.createTime as createTime, " +
            "l.name as languageName " +
            "FROM OrderTaxi ot " +
            "INNER JOIN ot.account as a " +
            "INNER JOIN ot.language as l " +
            "WHERE a.name LIKE ?1")
    Page<ClientOrderView> getOrdersFilterClientName(String name, Pageable pageable);
}
