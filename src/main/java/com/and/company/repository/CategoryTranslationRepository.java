package com.and.company.repository;

import com.and.company.model.CategoryTranslation;
import com.and.company.repository.projection.CostView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryTranslationRepository extends JpaRepository<CategoryTranslation, Long> {

    @Query("SELECT c FROM CategoryTranslation c " +
            "INNER JOIN c.language as l " +
            "WHERE l.name = ?1")
    List<CategoryTranslation> getAllCategoriesByLanguage(String languageName);

    @Query("SELECT c.costPerKM as costPerKM from CategoryTranslation c " +
            "INNER JOIN c.language as l " +
            "WHERE l.id = ?1 and c.description = ?2")
    CostView getCostPerKm(Long languageId, String description);
}
