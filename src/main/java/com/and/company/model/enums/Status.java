package com.and.company.model.enums;

public enum Status {
    AVAILABLE, INACTIVE, BUSY;

    public static Status getStatus(String statusName) {
        return Status.valueOf(statusName.toUpperCase());
    }

}
