package com.and.company.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table(name = "category")
public class Category {

    @EmbeddedId
    private CategoryKey categoryKey;

    @ToString.Exclude
    @ManyToOne
    @MapsId("carId")
    @JoinColumn(name = "car_id")
    private Car car;

    @ToString.Exclude
    @ManyToOne
    @MapsId("categoryTranslationId")
    @JoinColumn(name = "category_translation_id")
    private CategoryTranslation categoryTranslation;

}
