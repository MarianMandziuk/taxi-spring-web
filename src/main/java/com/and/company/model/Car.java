package com.and.company.model;

import com.and.company.model.enums.Status;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@NamedQuery(name = "Car.updateCarStatus",
            query = "UPDATE Car c SET c.status = :status WHERE c.id = :carId")
@NamedNativeQuery(name = "Car.findFirstCarByCapacityAndStatus",
            query = "SELECT car.* " +
                    "FROM car " +
                    "INNER JOIN category " +
                    "ON car.id = category.car_id " +
                    "INNER JOIN category_translation " +
                    "ON category_translation.id = category.category_translation_id " +
                    "WHERE category_translation.language_id = :languageId " +
                    "AND car.capacity >= :capacity " +
                    "AND car.status = :status " +
                    "AND category_translation.description = :category " +
                    "LIMIT 1;",
            resultClass = Car.class)
@NamedNativeQuery(name = "Car.getSeveralCarsByCapacity",
            query = "SELECT car1.* " +
                    "FROM car car1 " +
                    "LEFT JOIN car car2 ON car1.id >= car2.id " +
                    "INNER JOIN category " +
                    "ON car1.id = category.car_id " +
                    "INNER JOIN category_translation " +
                    "ON category_translation.id = category.category_translation_id " +
                    "WHERE category_translation.language_id = :languageId " +
                    "AND car1.status = :status " +
                    "AND category_translation.description = :category " +
                    "GROUP BY car1.id " +
                    "HAVING SUM(car2.capacity) <= (select sum(car2.capacity) AS capacity_sum " +
                        "FROM car car1 " +
                        "LEFT JOIN car car2 ON car1.id >= car2.id " +
                        "INNER JOIN category " +
                        "ON car1.id = category.car_id " +
                        "INNER JOIN category_translation " +
                        "ON category_translation.id = category.category_translation_id " +
                        "WHERE category_translation.language_id = :languageId " +
                        "AND car1.status = :status " +
                        "AND category_translation.description = :category " +
                        "GROUP BY car1.id " +
                        "HAVING capacity_sum >= :capacity " +
                        "LIMIT 1);",
            resultClass = Car.class)
@Data
@Entity
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String modelName;

    @Column(nullable = false)
    private Integer capacity;

    @Column(nullable = false)
    @Enumerated
    private Status status;

    @ToString.Exclude
    @ManyToMany(mappedBy = "carList")
    List<OrderTaxi> orderTaxiList;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "car")
    private List<Category> categoryList;
}
