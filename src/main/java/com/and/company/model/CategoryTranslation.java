package com.and.company.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Table(name = "category_translation")
public class CategoryTranslation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false, precision=5, scale=2)
    private BigDecimal costPerKM;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "categoryTranslation")
    private List<Category> categoryList;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "language_id")
    private Language language;
}
