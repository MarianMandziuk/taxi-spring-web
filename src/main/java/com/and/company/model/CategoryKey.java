package com.and.company.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class CategoryKey implements Serializable {

    @Column(name = "category_translation_id")
    private Long categoryTranslationId;

    @Column(name = "car_id")
    private Long carId;

}
