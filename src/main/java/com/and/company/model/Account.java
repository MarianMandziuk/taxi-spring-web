package com.and.company.model;

import com.and.company.model.enums.Role;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false, length = 32)
    private String password;

    @Column(nullable = false)
    private String name;


    @Column(name = "create_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createTime;

    @Column(columnDefinition = "integer default 21")
    @Enumerated
    private Role role;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "loyalty_id", referencedColumnName = "id")
    private Loyalty loyalty;

    @ToString.Exclude
    @OneToMany(mappedBy = "account")
    private List<OrderTaxi> orderTaxiList;
}
