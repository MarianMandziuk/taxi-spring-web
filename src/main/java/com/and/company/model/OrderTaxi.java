package com.and.company.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "order_taxi")
public class OrderTaxi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "passenger_number", nullable = false)
    private Integer passengerNumber;

    @Column(name = "destination_address", nullable = false)
    private String destinationAddress;

    @Column(name = "pickup_address", nullable = false)
    private String pickupAddress;

    @Column(nullable = false)
    private BigDecimal cost;

    @Column(name = "create_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createTime;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "language_id", referencedColumnName = "id")
    private Language language;

    @Column(name = "latitude_destination", nullable = false, precision=10, scale=6)
    private BigDecimal latitudeDestination;

    @Column(name = "longitude_destination", nullable = false, precision=10, scale=6)
    private BigDecimal longitudeDestination;

    @Column(name = "latitude_pickup", nullable = false, precision=10, scale=6)
    private BigDecimal latitudePickup;

    @Column(name = "longitude_pickup", nullable = false, precision=10, scale=6)
    private BigDecimal longitudePickup;

    @ToString.Exclude
    @ManyToMany
    @JoinTable(name = "order_has_car",
               joinColumns = @JoinColumn(name = "order_taxt_id"),
               inverseJoinColumns = @JoinColumn(name = "car_id"))
    private List<Car> carList;
}
