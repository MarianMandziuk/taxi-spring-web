package com.and.company.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "car_park_address")
public class CarParkAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String address;

    @Column(nullable = false, precision=10, scale=6)
    private BigDecimal longitude;

    @Column(nullable = false, precision=10, scale=6)
    private BigDecimal latitude;
}
