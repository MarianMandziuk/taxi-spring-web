package com.and.company.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Data
public class CarDto {
    private Long id;
    @NotBlank
    private String modelName;
    @Positive
    private Integer capacity;
    private Long statusId;
}
