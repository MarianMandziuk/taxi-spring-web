package com.and.company.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
public class CategoryTranslationDto {
    private Long id;
    @NotBlank
    private String description;
    @Positive
    private BigDecimal costPerKM;
    private Integer languageId;
}
