package com.and.company.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarParkAddressDto {

    private Long id;
    private String address;

    @Min(-180)
    @Max(180)
    private BigDecimal longitude;

    @Min(-90)
    @Max(90)
    private BigDecimal latitude;
}
