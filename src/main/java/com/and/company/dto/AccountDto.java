package com.and.company.dto;

import com.and.company.dto.group.Mandatory;
import com.and.company.dto.group.Secondary;
import com.and.company.validators.SamePassword;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@SamePassword(groups = Mandatory.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDto {
    private Long id;

    @NotNull(groups = Mandatory.class)
    @Email(groups = Mandatory.class)
    private String email;

    @NotNull(groups = Mandatory.class)
    @Size(min = 8, max = 32, groups = Mandatory.class)
    private String password;

    @NotNull(groups = Mandatory.class)
    @Size(min = 8, max = 32, groups = Mandatory.class)
    private String passwordRepeat;

    @Size(min = 2, groups = Secondary.class)
    private String name;
    private Long roleId;
    private Long loyaltyId;
}
