package com.and.company.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
public class OrderCompletedDto {
    @NotBlank
    private final String carModel;
    @Positive
    private final int waitMinutes;
    @NotBlank
    private final String pickUpAddress;
    @Positive
    private final BigDecimal cost;
    @NotBlank
    private final String language;
}
