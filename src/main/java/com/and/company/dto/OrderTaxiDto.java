package com.and.company.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderTaxiDto {
    private Long id;
    @Positive
    private Integer passengerNumber;
    private String destinationAddress;
    private String pickupAddress;
    @Positive
    private BigDecimal cost;
    private Long accountId;
    private Long languageId;
    @Min(-90)
    @Max(90)
    private BigDecimal latitudeDestination;
    @Min(-180)
    @Max(180)
    private BigDecimal longitudeDestination;
    @Min(-90)
    @Max(90)
    private BigDecimal latitudePickup;
    @Min(-180)
    @Max(180)
    private BigDecimal longitudePickup;
}
