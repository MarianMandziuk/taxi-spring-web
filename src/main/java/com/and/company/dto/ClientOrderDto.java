package com.and.company.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ClientOrderDto {
    private String name;
    private Integer passengerNumber;
    private String destinationAddress;
    private String pickupAddress;
    private BigDecimal cost;
    private LocalDateTime createTime;
    private String languageName;
}
