package com.and.company.dto;

import com.and.company.model.Language;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
public class LoyaltyDto {
    private Long id;
    @Positive
    private BigDecimal price;
    @Min(0)
    @Max(100)
    private Integer discount;
    private Language language;
}
