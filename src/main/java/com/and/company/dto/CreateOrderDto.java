package com.and.company.dto;

import lombok.Data;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
public class CreateOrderDto {
    @Positive
    private Integer passengerNumber;

    private String category;
    private String destinationAddress;
    private String pickupAddress;
    private Long accountId;
    @NotNull
    @Positive
    private Long languageId;

    @Min(-90)
    @Max(90)
    private BigDecimal latitudeDestination;
    @Min(-180)
    @Max(180)
    private BigDecimal longitudeDestination;

    @Min(-90)
    @Max(90)
    private BigDecimal latitudePickup;
    @Min(-180)
    @Max(180)
    private BigDecimal longitudePickup;
}
