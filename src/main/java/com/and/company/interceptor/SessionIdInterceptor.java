package com.and.company.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class SessionIdInterceptor implements HandlerInterceptor {

    private static final Logger log = LoggerFactory.getLogger(SessionIdInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String sessionId = request.getSession().getId();
        log.info("<pre handle interceptor> session id is {}", sessionId);
        return true;
    }
}
