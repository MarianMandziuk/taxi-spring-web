package com.and.company.util;

public class Distance {
    public static final int R_EARTH = 6371;
    public static final float KM_PER_MIN = 1.85f;

    public static int getDistanceFromLatLonInKm(double lat1, double lon1, double lat2, double lon2) {
        double dLat = deg2rad(lat2 - lat1);
        double dLon = deg2rad(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
                    * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R_EARTH * c;
        return (int) Math.ceil(d);
    }

    public static int minutesSpend(int km) {
        return (int) (KM_PER_MIN * km);
    }

    private static double deg2rad(double deg) {
        return deg * (Math.PI / 100d);
    }
}
